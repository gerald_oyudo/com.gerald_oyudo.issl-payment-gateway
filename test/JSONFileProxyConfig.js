var chai    = require("chai");
var expect = chai.expect;
var assert = chai.assert;
var fs = require("fs");
var JSONFileProxyConfig = require("../app/config/JSONFileProxyConfig");

var referenceOptions = {
			target: "http://localhost/",
			ssl: {
				key: fs.readFileSync('server.key', 'utf8'),
				cert: fs.readFileSync('server.crt', 'utf8')
			  },
			secure:false
				
			
};
describe("JSONFileProxyConfig Test", function(){
	before(function(done){
		createSample(function(){
			console.log("Sample created");
			done();
		},function(){
			console.log("create sample failed");
			assert.fail();
			done();
		})
	});
	
	it("Test creation", function(done){
		var proxyConfig = createConfig(function(data){
			console.log(data);
			done();
		}, done);
	});
	
	it("Test proxy read", function(done){
		var proxyConfig = createConfig(function(proxyConfig){
			assert.equal(8080, proxyConfig.getPort());
			done();
		}, done);
	});
	
	it("Test other config read", function(done){
		createConfig(function(proxyConfig){
			var options = {};
			options.target = proxyConfig.getConfig("target");
			options.secure = proxyConfig.getConfig("secure");
			options.ssl = {};
			proxyConfig.getSSLKey(function(key){
				
				options.ssl.key = key;
				proxyConfig.getSSLCert(function(cert){
					options.ssl.cert = cert
					assert.deepEqual(referenceOptions, options);
					done();
				}, function(err){
					console.log(err);
					assert.fail();
					done();
				});
				
				
			}, function(err){
				console.log(err);
				assert.fail();
				done();
			});
			
		}, done);
	});
});


var createConfig = function(success, done){
	
	JSONFileProxyConfig.create("sample1.json",
		function(data){
			success(data);
		},function(err){
			console.log(err);
			assert.fail();
			done();
		});
}
var createSample = function(success, failure){
	 var exists = fs.existsSync("sample1.json");
	if(!exists){
		
		var sample = {
			proxy: {
				port: 8080,
				options: referenceOptions
			}
			
		}
		
		// Create a writable stream
		var writerStream = fs.createWriteStream('sample1.json');
		// Write the data to stream with encoding to be utf8
		writerStream.write(JSON.stringify(sample),'UTF8');
		// Mark the end of file
		writerStream.end();
		// Handle stream events --> finish, and error
		writerStream.on('finish', function() {
			console.log("Write completed.");
			success();
		});

		writerStream.on('error', function(err){
		   console.log("Reading error");
		   
		   failure();
		});
	}else{
		success();
	}
}