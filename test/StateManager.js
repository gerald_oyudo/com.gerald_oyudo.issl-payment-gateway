var StateManager = require("../app/utils/StateManager.js");
var chai    = require("chai");
var assert = chai.assert;

describe("StateManager test", function(){
	it("Testing valid paths", function(){
		var stateManager = StateManager.create();
		console.log(stateManager);
		assert.equal(stateManager.getState("/pending"),"pending");
		assert.equal( stateManager.getState("/success"), "success");
		assert.equal(stateManager.getState("/cancel"), "cancel");
		assert.equal(stateManager.getState("/failure"), "failure");
	});
	
	it("testing invalid paths", function(){
		var stateManager = StateManager.create();
		assert.equal(stateManager.getState("/"), null);
		assert.equal( stateManager.getState("/soc"), null);
		assert.equal( stateManager.getState("/cancol"), null);
		assert.equal( stateManager.getState("/failure/good"), null);
	});
});