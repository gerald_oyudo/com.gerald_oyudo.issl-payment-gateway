var chai    = require("chai");
var expect = chai.expect;
var assert = chai.assert;
var fs = require("fs");
var JSONFileReader = require("../app/utils/JSONFileReader");
console.log(JSONFileReader);

describe('hooks', function() {

  before(function(done) {
    var exists = fs.existsSync("sample.json");
	if(!exists){
		var sample = {sample: {data:"Data 1", otherData:"Data 2"}};
		// Create a writable stream
		var writerStream = fs.createWriteStream('sample.json');
		// Write the data to stream with encoding to be utf8
		writerStream.write(JSON.stringify(sample),'UTF8');
		// Mark the end of file
		writerStream.end();
		// Handle stream events --> finish, and error
		writerStream.on('finish', function() {
			console.log("Write completed.");
			done();
		});

		writerStream.on('error', function(err){
		   console.log("Reading error");
		   assert.fail();
		   done();
		});
	}else{
		done();
	}
	
  });

  after(function() {
    // runs after all tests in this block
	console.log("after");
  });

  beforeEach(function() {
    // runs before each test in this block
	console.log("beforeEach");
  });

  afterEach(function() {
    // runs after each test in this block
	console.log("afterEach");
  });

  // test cases
  describe("JSON File reader", function(){
	
	it("reads valid object from file (with sample key)", function(done){
		var jReader = JSONFileReader.JSONFileReader("sample.json");
		
		jReader.readObject("sample",function(data){
			console.log(data);
			done();
		}, function(err){
			console.log(err);
			
			assert.fail(false, "Reading returned an error");
			done();
		})
	});
	
	
	
});
});

