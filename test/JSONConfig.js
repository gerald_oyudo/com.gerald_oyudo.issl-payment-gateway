var chai    = require("chai");
var expect = chai.expect;
var assert = chai.assert;
var JSONConfig = require("../app/utils/JSONConfig");
var fs = require("fs");

describe("JSONConfig", function(){
	before(function(done){
		createSample(function(){
			console.log("Sample created");
			done();
		}, function(){
			console.log("Sample creation failed");
			assert.fail();
			done();
		});
	});
	
	it("Test object creation", function(done){
		var jsonConfig = createConfig( function(jsonConfig){
			console.log(jsonConfig);
			done();
		},done);
		
	});
	it("Test parameter getting", function(done){
		var jsonConfig = createConfig( function(jsonConfig){
			assert.equal("Data 1", jsonConfig.getValue("data"), "Object not getting proper value");
			assert.equal("Data 2", jsonConfig.getValue("otherData"), "Object not getting proper value");
			done();
		},done);
		
		

	});
	
});

var createSample = function(success, failure){
	 var exists = fs.existsSync("sample.json");
	if(!exists){
		var sample = {sample: {data:"Data 1", otherData:"Data 2"}};
		// Create a writable stream
		var writerStream = fs.createWriteStream('sample.json');
		// Write the data to stream with encoding to be utf8
		writerStream.write(JSON.stringify(sample),'UTF8');
		// Mark the end of file
		writerStream.end();
		// Handle stream events --> finish, and error
		writerStream.on('finish', function() {
			console.log("Write completed.");
			success();
		});

		writerStream.on('error', function(err){
		   console.log("Reading error");
		   
		   failure();
		});
	}else{
		success();
	}
}

var createConfig = function(success, done){
	
	return JSONConfig.JSONConfig("sample.json","sample",
		function(data){
			success(data);
		},function(err){
			console.log(err);
			assert.fail();
			done();
		});
}