var chai    = require("chai");
var expect = chai.expect;
var assert = chai.assert;
var fs = require("fs");
var JSONFileResourceConfig = require("../app/config/JSONFileResourceConfig");

var sites = {
		"sample" : {
			"Urls":{
				"pending": "http://localhost:8085/pending.html",
				"cancel": "http://localhost:8085/cancel.html",
				"success": "http://localhost:8085/success.html",
				"failure":"http://localhost:8085/failure.html"
			}
		}
		
};
describe("JSONFileResourceConfig Test", function(){
	before(function(done){
		createSample(function(){
			console.log("Sample created");
			done();
		},function(){
			console.log("create sample failed");
			assert.fail();
			done();
		})
	});
	
	it("Test creation", function(done){
		createConfig(function(data){
			console.log(data);
			done();
		}, done);
	});
	
	it("Test proxy read", function(done){
		createConfig(function(resourceConfig){
			assert.equal(sites.sample.Urls.pending, 
			resourceConfig.getUrl("sample","pending"));
			assert.equal(sites.sample.Urls.cancel, 
			resourceConfig.getUrl("sample","cancel"));
			assert.equal(sites.sample.Urls.success, 
			resourceConfig.getUrl("sample","success"));
			assert.equal(sites.sample.Urls.failure, 
			resourceConfig.getUrl("sample","failure"));
			assert.equal(8080, 
			resourceConfig.getPort());
			done();
		}, done);
	});
	
	
	
});


var createConfig = function(success, done){
	
	JSONFileResourceConfig.create("sample2.json",
		function(data){
			success(data);
		},function(err){
			console.log(err);
			assert.fail();
			done();
		});
}

var createSample = function(success, failure){
	 var exists = fs.existsSync("sample2.json");
	if(!exists){
		
		var sample = {
			resource: {
				"port": 8080,
				"sites": sites
			}
			
		}
		
		// Create a writable stream
		var writerStream = fs.createWriteStream('sample2.json');
		// Write the data to stream with encoding to be utf8
		writerStream.write(JSON.stringify(sample),'UTF8');
		// Mark the end of file
		writerStream.end();
		// Handle stream events --> finish, and error
		writerStream.on('finish', function() {
			console.log("Write completed.");
			success();
		});

		writerStream.on('error', function(err){
		   console.log("Reading error");
		   
		   failure();
		});
	}else{
		success();
	}
}

