var ClientResourceServer = require("../app/ClientResourceServer.js");
var PostDataSiteIdManager = require("../app/utils/PostDataSiteIdManager");
var request = require("request");
var chai    = require("chai");
var assert = chai.assert;
var clientResourceServer = {};
var clientServer;
var serverPort = 8080;
var siteIDReference = "sample";
describe("ClientResourceServer test", function(){
	describe("Object Creation",function(){
		it("Creating ClientResourceServer", function(done){
			
			ClientResourceServer.create(function(rServer){
				var cs = rServer;
				console.log(cs);
				done();
			}, function(err){
				console.log(err);
				assert.fail();
				done();
			});
			
		});
	});
	
	describe("Testing server startup and shutdown", function(){
		it("Creating, starting and stopping server", function(done){
			this.timeout(5000);
			ClientResourceServer.create(function(rServer){
				
				rServer.start();
				
				setTimeout(function(){
					rServer.stop();
					done();
				}, 2000);
			}, function(err){
				console.log(err);
				assert.fail();
				done();
			});
			
		});
		
	});
	
	describe("Testing server functionalities", function(){
		before("Creating and starting server", function(done){
			ClientResourceServer.create(function(rServer){
				clientResourceServer = rServer;
				clientResourceServer.start();
				done();
			}, function(err){
				console.log(err);
				assert.fail();
				done();
			});
			
		});
		after("Stopping server", function(){
			clientResourceServer.stop();
		});
		
		it("Sending a post request with site id embedded path == /pending", function(done){
			sendRequest("/pending",done);
		});
		it("Sending a post request with site id embedded path == /cancel", function(done){
			sendRequest("/cancel",done);
		});
		it("Sending a post request with site id embedded path == /success", function(done){
			sendRequest("/success",done);
		});
		it("Sending a post request with site id embedded path == /failure", function(done){
			sendRequest("/failure",done);
		});
		it("Sending a post request with site id embedded path == /success with query params", function(done){
			sendRequest("/success?id=23423",done);
		});
	});
});

var sendRequest = function(path,done){
	var formData = {data1: "Data 1", data2: "Data 2"};
			console.log("Before");
			console.log(formData);
			var postDataSiteIDHandler = PostDataSiteIdManager.create();
			formData = postDataSiteIDHandler.appendSiteId(siteIDReference, formData);
			console.log("After");
			console.log(formData);
			request.post('http://localhost:' + serverPort + path,
						{form: formData}, function (error, response, body) {
				  if(error){
					  console.log(error);
					  assert.fail();
					  done();
				  }
				  
				  console.log(response.body);
				  console.log("Response gotten");
				  
				  done();
			});
}