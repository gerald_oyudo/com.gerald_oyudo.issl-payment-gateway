var PaymentProxyServer = require("../app/PaymentProxyServer");
var chai    = require("chai");
var assert = chai.assert;
var request = require('request');

var proxy={};
describe("PaymentProxyServer Test", function(){
	
	describe("Creation", function(){
		it("Create paymentProxyServer", function(done){
			
			PaymentProxyServer.create(function(proxyServer){
				console.log(proxyServer);
				done();
			}, function(err){
				console.log(err);
				assert.fail();
				done();
			});
		});
		
	});
	
	describe("Starting and Stopping", function (){
		before(function(done){
			
			PaymentProxyServer.create(function(proxyServer){
				proxy = proxyServer;
				proxy.setErrorListener(handleProxyError);
				proxy.setResponseListener(handleProxyResponse);
				done();
			}, function(err){
				console.log(err);
				assert.fail();
				done();
			});
		});
		
		
		it("Start Payment Proxy Server", function(){
			proxy.start();
		});
		it("Wait 3 secs then ping proxy server", function(done){
			this.timeout(5000);
			setTimeout(function(){
				request('http://localhost:8086', function (error, response, body) {
				  console.log("CONTROL -- DESIRED RESULT");
				  if(error){
					  
					  console.log(error);
					  assert.fail();
				  }
				  console.log(body);
				  
				});
				request('https://localhost:8085', function (error, response, body) {
				  if(error){
					  console.log(error);
					  assert.fail();
				  }
				  console.log(body);
				  done();
				});
			}, 3000);
			
		});
		it("Stop proxyServer", function(){
				proxy.stop();
				
		});
		it("Test if server has stopped", function(){
			request('https://localhost:8085', function (error, response, body) {
				  if(error){
					  console.log(error);
					  done();
				  }
				console.log("Server has not stopped");
				  assert.fail();
				});
		})
	})
});

var handleProxyResponse = function (proxyRes,req, res){
	console.log("Response gotten from proxy");
}
var handleProxyError = function (err, req, res){
	console.log("Proxy error gotten");
}
			
