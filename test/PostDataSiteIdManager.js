var PostDataSiteIdManager = require("../app/utils/PostDataSiteIdManager");
var chai    = require("chai");
var assert = chai.assert;
var request = require('request');
var http = require('http');
var querystring = require('querystring');
var testServerPort = 8081;
var siteIDReference = "sample";
var server;

describe("PostDataSiteIdManager Test", function(){
	describe("SiteIdAppending Process", function(){
		before("Creating PostDataSiteIdManager and starting test Server", function(){
			console.log("Executing before");
			createHttpServer();
		});
		after("Closing Server", function(done){
			console.log("Executing after");
			server.close(function(){
				done();
			});
		})
		it("test appending and extracting of siteId", function(done){
			var formData = {data1: "Data 1", data2: "Data 2"};
			console.log("Before");
			console.log(formData);
			var postDataSiteIdManager = PostDataSiteIdManager.create();
			formData = postDataSiteIdManager.appendSiteId(siteIDReference, formData);
			console.log("After");
			console.log(formData);
			request.post('http://localhost:' + testServerPort,
						{form: formData}, function (error, response, body) {
				  if(error){
					  console.log(error);
					  assert.fail();
				  }
				  console.log("Body ==== " + body);
				  if(body !== "true"){
					  assert.fail();
				  }
				  done();
			});
		});
	});
});

var createHttpServer = function(){
	server = http.createServer(function(request,resp){
				var postDataSiteIdManager = PostDataSiteIdManager.create();			
				if (request.method === 'POST') {
					var postData = null;
					var jsonString = '';
					request.on('data', function (data) {
						
						jsonString += data;
					});
					request.on('error', function(err){
						console.log(err);
					});
					request.on('end', function () {
						console.log("JSON String");
					console.log(jsonString);
						
						postData = querystring.parse(jsonString);
						
						
						 console.log("On receive");
						console.log(postData);
						var siteId = postDataSiteIdManager.extractSiteId(postData);
						if(siteId !== siteIDReference){
							resp.write("false");
						}else{
							resp.write("true");
						}
						
						resp.end();
					});
					
				}else{
						resp.write("Does not support Get");
						
				}
				
				
			});
	server.listen(testServerPort);
}