var URLIDHandler = require("../app/utils/PostDataRequestIDHandler");
var PostDataSiteIdManager = require("../app/utils/PostDataSiteIdManager");

var chai    = require("chai");
var assert = chai.assert;
var request = require('request');
var http = require('http');
var querystring = require('querystring');
var testServerPort = 8081;
var siteIDReference = "sample";
var server;
var express = require('express');
var app = express();
var bodyParser = require('body-parser');



describe("URLIDHandler Test", function(){
	describe("SiteIdAppending Process", function(){
		before("Creating URLIDHandler and starting test Server", function(){
			console.log("Executing before");
			createHttpServer();
		});
		after("Closing Server", function(done){
			console.log("Executing after");
			server.close(function(){
				done();
			});
		})
		it("test appending and extracting of siteId", function(done){
			var formData = {data1: "Data 1", data2: "Data 2"};
			console.log("Before");
			console.log(formData);
			var postDataSiteIDHandler = PostDataSiteIdManager.create();
			formData = postDataSiteIDHandler.appendSiteId(siteIDReference, formData);
			console.log("After");
			console.log(formData);
			request.post('http://localhost:' + testServerPort,
						{form: formData}, function (error, response, body) {
				  if(error){
					  console.log(error);
					  assert.fail();
				  }
				  console.log("Body ==== " + body);
				  if(body !== "true"){
					  assert.fail();
				  }
				  done();
			});
		});
	});
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));
app.all('/*',function(req,res){
	var urlIDHandler = URLIDHandler.create();
	var siteId = urlIDHandler.processRequest(req);
	console.log("Site id returned by urlIdHandler");
	console.log(siteId);
	if(siteId !== siteIDReference){
		res.write("false");
	}else{
		res.write("true");
	}
	
	res.end();
});

var createHttpServer = function(){
	
	server = app.listen(testServerPort, function () {


	console.log(" Server  listening ");

	});
	
}