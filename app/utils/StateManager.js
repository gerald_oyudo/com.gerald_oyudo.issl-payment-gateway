/*
StateManager
Defines the states in transaction lifecycle of the payment. 
It also maps it with their corresponding urls.
More urls can be mapped to existing state, or modified by 
changing the value of the stateDictionary.
*/
exports.create = function(){
	
	// path - state mapping
	var stateDictionary = {
		"/pending": "pending",
		"/failure": "failure",
		"/success": "success",
		"/cancel": "cancel"
	};
	
	var ret = {
		/*
		getState
		returns the state corresponding to the url path.
		@param1 relative request url path.
		@returns the state.
		*/
		getState: function(path){
			var state = stateDictionary[path];
			if(!state)
				return null;
			else
				return state;
		}
	}
	return ret;
}