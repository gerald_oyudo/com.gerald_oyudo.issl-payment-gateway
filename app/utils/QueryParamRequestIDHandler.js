/*
PostDataRequestIDHandler
Extracts SiteId from a data. 
This module can only accept request from an express server using
a body parser. 
Redifine postData extraction if intended to use with other servers.
*/

var JsonDataSiteIDManager = require( __dirname + "/DiamondBankSiteIdManager.js");

exports.create = function(){
	var jsonDataSiteIDManager = JsonDataSiteIDManager.create();
	ret = {
		processRequest: function(request){
			var data = request.query;
			var siteID = jsonDataSiteIDManager.extractSiteId(data);
			return siteID;
		}
	}
	return ret;
}