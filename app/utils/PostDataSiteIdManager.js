/*
 PostDataSiteIdManager
 Extracts the siteId information to an object
 The object can be gotten using postData from POST request
 or parsing query string from a get request
 using a specified post data first level key.
 It appends the site id using a delimiter.
**/

exports.create = function(){
	var postDataKey = "pname";
	var delimiter = ":";
	
	var ret = {
		/*
		appendSiteId
		appends the siteId to the object
		@param1 site id to append
		@param2 post data to attach to.
		@returns the resultant object
		*/
		appendSiteId: function(siteId, postData){
			if(!postData[postDataKey]){
				postData[postDataKey] = "";
			}
			if((typeof postData[postDataKey]) !== "string"){
				postData[postDataKey] = "";
			}
			postData[postDataKey] = siteId + delimiter +postData[postDataKey];
			return postData;
		},
		/*
		extractSiteId
		extracts the site id from an object using the
		specified keyname and delimiter
		@param1 the object to extract from
		@returns the site Id or null if there is no key or value.
		*/
		extractSiteId: function(postData){
			if(!postData || !postData[postDataKey]){
				
				return null;
			}
			var keyValue = postData[postDataKey];
			if((typeof keyValue) !== "string"){
				
				return null;
			}
			var siteId = keyValue.split(delimiter)[0];
			return siteId;
		}
	}
	return ret;
}