/*
JSONConfig
This provides an interface for reading configuration values 
from a file.
*/
var jReader = require(__dirname + "/JSONFileReader.js");

/*
JSONConfig
Creates the JSONConfig object
@param1 the path name of the file
@param2 the first level key of the object
@param3 success callback that accepts the JSONConfig object
@param4 failure callback that accepts the failure message
*/
exports.JSONConfig = function(pathName, rootKey, success, failure){
	
	if(!pathName || !rootKey){
		failure("No pathname or rootkey supplied");
	}
	
	var reader = jReader.JSONFileReader(pathName);
	var configObject = {};
	
	reader.readObject(rootKey,function(data){
		
		if(!data[rootKey]){
			failure("No data corresponding to root key")
		}
		
		configObject = data[rootKey];
		
		var ret = {
			/*
			getValue
			returns an object property specified with the key.
			returns undefined if key is absent.
			*/
			getValue: function(key){
				return configObject[key];
		}};
		
		success(ret);
		
	},function(err){
		
		failure(err);
		
	});
}
