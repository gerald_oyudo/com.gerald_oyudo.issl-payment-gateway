//JSONFileReader


/*
JSONFileReader
reads a json object from a file given the first level key value.
*/
( function () {
	var fs = require('fs');
	/*
	JSONFileReader
	creates a JSONFileReader with the path to the file supplied.
	*/
	exports.JSONFileReader = function(pathName){
		
			
		return {
				/*
				Reads the object from a file,
				@param1 first level object key
				@param2 success callback function 
				that accepts the read object
				@param3 failure callback function 
				that accepts the error object
				*/
				readObject: function(key, success, failure){
					if(!key){
						failure("no key supplied");
						
					}
					else{
						fs.readFile(pathName,"utf8",function(err,data){
							if(err){
								failure(err);
							}else{
								var data = JSON.parse(data);
								if(!data[key]){
									failure("No data with specified key");
								}else{
									success(data);
								}
							}
						});
					}
				}
		};
	
	};
} )();