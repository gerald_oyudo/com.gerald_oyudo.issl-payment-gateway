/*
PostDataRequestIDHandler
Extracts SiteId from a data. 
This module can only accept request from an express server using
a body parser. 
Redifine postData extraction if intended to use with other servers.
*/

var PostDataSiteIDManager = require( __dirname + "/PostDataSiteIdManager.js");

exports.create = function(){
	var postDataSiteIdManager = PostDataSiteIDManager.create();
	ret = {
		processRequest: function(request){
			var postData = request.body; // redefine if intended to be used with other servers
			var siteID = postDataSiteIdManager.extractSiteId(postData);
			return siteID;
		}
	}
	return ret;
}