var JSONFileProxyConfig = require(__dirname + "/config/JSONFileProxyConfig.js");
var httpProxy = require("http-proxy");
var DataRequestIDHandler = require(__dirname + "/utils/QueryParamRequestIDHandler.js");
var StateManager = require(__dirname + "/utils/StateManager.js");
var JSONFileResourceConfig = require(__dirname + "/config/JSONFileResourceConfig.js");
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var querystring = require('querystring');
var serverPort;
var https = require('https');
var httpsServer;


process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

exports.create = function(success,failure){
	JSONFileProxyConfig.create( __dirname + "/properties.json",function(proxyConfig){
			var options = proxyConfig.getOptions();
			options.ssl = {};
			
			proxyConfig.getSSLKey( function(sslKey){
			options.ssl.key = sslKey;
			
				proxyConfig.getSSLCert(function(sslCert){
						options.ssl.cert = sslCert;
						options.checkServerIdentity = checkClientIdentity;
						app.all('/*',function(req,res){
							console.log("Request");
							var url = options.target;
							var qstring = querystring.stringify(req.query);
							console.log(qstring);
							var combinedPath = url + (qstring?"?":"") + qstring;
							console.log(combinedPath);
							res.redirect(307, combinedPath);
						
						});
						var ret = {
							start: function(){
								console.log("Https Server started on " + proxyConfig.getPort());
								httpsServer = https.createServer(options.ssl, app);
								httpsServer.listen(proxyConfig.getPort());
							},
							stop: function(){
								httpsServer.close();
							},
							setErrorListener:function(errorFn){
								
							},
							setResponseListener:function(responseFn){
								
							}
						}
						success(ret);
					}, function(err){
						failure(err);
				});
			
			}, function(err){
			failure(err);
		});
		
	}, function(err){
		
		failure(err);
	});
}

var checkClientIdentity = function(host,cert){
	return true;
}


