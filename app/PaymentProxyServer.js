var JSONFileProxyConfig = require(__dirname + "/config/JSONFileProxyConfig.js");
var httpProxy = require("http-proxy");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

exports.create = function(success,failure){
	JSONFileProxyConfig.create( __dirname + "/properties.json",function(proxyConfig){
			var options = proxyConfig.getOptions();
			options.ssl = {};
			
			proxyConfig.getSSLKey( function(sslKey){
			options.ssl.key = sslKey;
			
				proxyConfig.getSSLCert(function(sslCert){
						options.ssl.cert = sslCert;
						options.checkServerIdentity = checkClientIdentity;
						var proxy = httpProxy.createProxyServer(options);
						var ret = {
							start: function(){
								proxy.listen(proxyConfig.getPort());
							},
							stop: function(){
								proxy.close();
							},
							setErrorListener:function(errorFn){
								proxy.on('error', errorFn);
							},
							setResponseListener:function(responseFn){
								proxy.on('proxyRes', responseFn);
							}
						}
						success(ret);
					}, function(err){
						failure(err);
				});
			
			}, function(err){
			failure(err);
		});
		
	}, function(err){
		
		failure(err);
	});
}

var checkClientIdentity = function(host,cert){
	return true;
}


