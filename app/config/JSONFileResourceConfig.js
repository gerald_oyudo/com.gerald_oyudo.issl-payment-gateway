var JSONConfig = require("../utils/JSONConfig.js");
var fs = require("fs");
var ROOT_KEY = "resource";


exports.create = function(path,success,failure){
	JSONConfig.JSONConfig(path,ROOT_KEY,function(config){
		
		var ret = {
			getPort: function(){
				return config.getValue("port");
			},
			getUrl: function(siteName, state){
				return config.getValue("sites")[siteName].Urls[state];
			}
			};
		success(ret);
	},function(err){
		console.log(err);
		failure(err);
	})
}
