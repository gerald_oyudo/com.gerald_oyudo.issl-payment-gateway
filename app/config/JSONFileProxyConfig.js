var JSONConfig = require("../utils/JSONConfig.js");
var fs = require("fs");
var ROOT_KEY = "proxy";
var SERVER_KEY_FILE_NAME = "server.key";
var SERVER_CERT_FILE_NAME = "server.crt";

exports.create = function(path,success,failure){
	JSONConfig.JSONConfig(path,ROOT_KEY,function(config){
		
		var ret = {
			getPort: function(){
				return config.getValue("port");
			},
			getSSLKey: function(success, failure){
				
				getServerKey(success,failure);
			},
			getSSLCert: function(success,failure){
				getServerCertificate(success,failure);
			},
			getConfig: function(name){
				return config.getValue("options")[name];
			},
			getOptions: function(){
				return config.getValue("options");
			}
			};
		success(ret);
	},function(err){
		console.log(err);
		failure(err);
	})
}

var getServerCertificate = function(success, failure){
	
	fs.readFile(SERVER_CERT_FILE_NAME,"utf8",function(err,data){
		if(err){
			failure(err);
		}else{
			success(data);
		}
	});
};
var getServerKey = function(success, failure){
	fs.readFile(SERVER_KEY_FILE_NAME,"utf8",function(err,data){
		if(err){
			failure(err);
		}else{
			success(data);
		}
	});
};
