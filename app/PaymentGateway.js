var PaymentServer = require(__dirname + "/ClientProcessServer.js");
var ClientResourceServer = require(__dirname + "/ClientResourceServer.js");

ClientResourceServer.create(function(rServer){
	PaymentServer.create(function(pServer){
		rServer.start();
		pServer.start();
		console.log("Client and server listening");
		
	}, function(err){
		console.log("Could not create server");
		console.log(err);
		process.exit(1);
	})
}, function(err){
	console.log("Could not create server");
	console.log(err);
	process.exit(1);
})

