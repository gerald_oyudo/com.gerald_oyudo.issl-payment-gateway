var DataRequestIDHandler = require(__dirname + "/utils/QueryParamRequestIDHandler.js");
var StateManager = require(__dirname + "/utils/StateManager.js");
var JSONFileResourceConfig = require(__dirname + "/config/JSONFileResourceConfig.js");
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var querystring = require('querystring');
var serverPort;


exports.create = function(success,failure){
	var dataRequestHandler = DataRequestIDHandler.create();
	var stateManager = StateManager.create();
	app.use(bodyParser.urlencoded({ extended: false }));
	
	JSONFileResourceConfig.create( __dirname + "/properties.json", function(resourceConfig){
		
		app.all('/*',function(req,res){
			
			if(req.method === "POST" || req.method === "GET"){
				
				var urlIDHandler = DataRequestIDHandler.create();
				var siteId = urlIDHandler.processRequest(req);
				if(siteId === null || siteId === 'undefined' || siteId=== ""){
					res.write("SiteId is not available or recoginzed");
					res.end();
					
				}else{
					
					//console.log("path");
					//console.log(req.path);
					var state = stateManager.getState(req.path);
					//console.log(state);
					var url = resourceConfig.getUrl(siteId,state);
					//console.log(url);
					var qstring = querystring.stringify(req.query);
					//console.log(qstring);
					var combinedPath = url + (qstring?"?":"") + qstring;
					//console.log(combinedPath);
					res.redirect(307, combinedPath);
					
				}
				
			}else{
				res.write("unsupported.");
				res.end();
			}
		
		});
		serverPort = resourceConfig.getPort();
		
		var ret = {
			start: function(){
				createHttpServer();
			},
			stop: function(){
				stopHttpServer();
			}
		}
		success(ret);
		
	}, function(err){
		failure(err);
	})
	
	
	
	
}



var createHttpServer = function(){
	server = app.listen(serverPort, function () {
	console.log(" Server  listening at " + serverPort);
	});

}
var stopHttpServer = function(){
	server.close();
}
